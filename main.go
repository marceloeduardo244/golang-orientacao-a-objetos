package main

import (
	"fmt"

	"gitlab.com/marceloeduardo244/banco/entidades"
	"gitlab.com/marceloeduardo244/banco/services"
)

func main() {
	contaDoDenis := entidades.ContaPoupanca{}
	contaDoDenis.Depositar(100)
	contaDoDenis.Sacar(55)
	fmt.Println(contaDoDenis.BuscaSaldo())

	services.PagarBoleto(&contaDoDenis, 55)

	contaDaLuisa := entidades.ContaCorrente{}
	contaDaLuisa.Depositar(1000)
	contaDaLuisa.Sacar(550)
	fmt.Println(contaDaLuisa.BuscaSaldo())

	services.PagarBoleto(&contaDaLuisa, 200)
}
