package services

import "gitlab.com/marceloeduardo244/banco/util"

func PagarBoleto(conta util.ValidarConta, valorDoBoleto float64) {
	conta.Sacar(valorDoBoleto)
}
