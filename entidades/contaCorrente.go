package entidades

type ContaCorrente struct {
	Titular TitularModel
	Agencia int
	Conta   int
	saldo   float64
}

func (c *ContaCorrente) Sacar(valorDoSaque float64) string {
	podeSacar := valorDoSaque <= c.saldo && valorDoSaque > 0
	if podeSacar {
		c.saldo -= valorDoSaque
		return "Saque autorizado!"
	} else {
		return "Saldo insuficiente..."
	}
}

func (c *ContaCorrente) Depositar(valorDoDeposito float64) (string, float64) {
	podeSacar := valorDoDeposito > 0
	if podeSacar {
		c.saldo += valorDoDeposito
		return "Deposito autorizado!", c.saldo
	} else {
		return "Valor inválido...", c.saldo
	}
}

func (c *ContaCorrente) Transferir(valorDaTransferencia float64, contaDestino *ContaCorrente) bool {
	if valorDaTransferencia < c.saldo && valorDaTransferencia > 0 {
		c.saldo -= valorDaTransferencia
		contaDestino.Depositar(valorDaTransferencia)

		return true
	} else {
		return false
	}
}

func (c *ContaCorrente) BuscaSaldo() float64 {
	return c.saldo
}
