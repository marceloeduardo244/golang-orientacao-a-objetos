package entidades

type ContaPoupanca struct {
	Titular  TitularModel
	Agencia  int
	Conta    int
	Operacao int
	saldo    float64
}

func (c *ContaPoupanca) Sacar(valorDoSaque float64) string {
	podeSacar := valorDoSaque <= c.saldo && valorDoSaque > 0
	if podeSacar {
		c.saldo -= valorDoSaque
		return "Saque autorizado!"
	} else {
		return "Saldo insuficiente..."
	}
}

func (c *ContaPoupanca) Depositar(valorDoDeposito float64) (string, float64) {
	podeSacar := valorDoDeposito > 0
	if podeSacar {
		c.saldo += valorDoDeposito
		return "Deposito autorizado!", c.saldo
	} else {
		return "Valor inválido...", c.saldo
	}
}

func (c *ContaPoupanca) BuscaSaldo() float64 {
	return c.saldo
}
